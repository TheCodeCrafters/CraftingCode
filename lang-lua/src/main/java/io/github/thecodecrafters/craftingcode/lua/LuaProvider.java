package io.github.thecodecrafters.craftingcode.lua;

import io.github.thecodecrafters.craftingcode.langapi.LanguageProvider;

public class LuaProvider implements LanguageProvider {
	@Override
	public String getLanguageName() {
		return "LUA";
	}
}
